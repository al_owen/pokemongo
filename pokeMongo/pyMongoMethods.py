from mongoengine.queryset.transform import update
import pymongo
from pymongo import MongoClient
from pprint import pprint

# ----------------------------- conection pymongo ---------------------------- #
uri = "mongodb+srv://root:super3@clusterm6.c7eiz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
client = MongoClient(uri)

db = client["project"]
pokemons = db["pokemon"]
teams = db["team"]
players = db["player"]

# ---------------------------------- methods --------------------------------- #


def pokeQuery(inLine):
    if inLine[2] is not None:
        doc = getPokemon(inLine[1], inLine[2])
        if doc is not None:
            tupladoc = (doc["name"], doc[inLine[2]])
            pprint(tupladoc)
        else:
            print("Los datos ingresados no son correctos, intente de nuevo")
    else:
        print("Los datos ingresados no son correctos, intente de nuevo")

# ------------------------- detach pokemon from team ------------------------- #

def detach(pokemonName):
    pokemon = None
    pokeName = getPokemon(pokemonName, None)
    try:
        print("Borrar Pokemon: ")
        if pokemonName.isdigit():
            pokeBorradoNum = teams.delete_one({"num": pokemonName.zfill(3)})
        else:
            pokeBorradoNum = teams.delete_one({"name": pokemonName.capitalize()})
        available = pokemonsInTeam()
        print(pokeName["name"]+" se ha borrado del equipo. Numero de Pokemons:",available)
    except:
        print("El pokemon no existe")
        
# ------------------- buscar el pokemo por nombre o numero ------------------- #

def getPokemon(name, field):
    doc = None
    if field is not None:
        if name.isdigit():
            doc = pokemons.find_one({"num": name.zfill(3)}, {"_id": 0, "name": 1, field: 1})
        elif name.isalpha():
            doc = pokemons.find_one({"name": name.capitalize()}, {"_id": 0, "name": 1, field: 1})
        else:
            print("error")
    else:
        if name.isdigit():
            doc = pokemons.find_one({"num": name.zfill(3)}, {"_id": 0, "name": 1})
        elif name.isalpha():
            doc = pokemons.find_one({"name": name.capitalize()}, {"_id": 0, "name": 1})
        else:
            print("error")
    return doc

# ------------------ contar la cantidad de pokemons en team ------------------ #

def pokemonsInTeam():
    counter = teams.count_documents({"_cls" : "Team"})
    return counter

# ------------------- añade 5 puntos al usuario en cuestion ------------------ #

def addScore(alias):
    player = players.find_one({"alias": alias}, {"_id":0, "alias":1, "score":1})
    if player is not None:
        newScore = player["score"] + 5
        players.update_one({"alias":alias}, {"$set":{"score": newScore}})
        print("Puntuaccion aumentada para", alias + ". Puntos actuales :", newScore)
    else:
        print("El usuario no existe")
