from mongoengine.connection import connect
from classes import *
import mongoengine
from mongoengine import *
from mongoengine import Document
from pprint import pprint

# -------------------------------- Connection -------------------------------- #

uri = "mongodb+srv://root:super3@clusterm6.c7eiz.mongodb.net/project?retryWrites=true&w=majority"
client = connect(host=uri)


# --------------------------------- FastMoves -------------------------------- #

steelWing = FastMove(name='Steel Wing', pwr=11, moveType="Steel")
steelWing.energyGain = 6
steelWing.save()

dragonTail = FastMove(name='Dragon Tail', pwr=15, moveType="Dragon")
dragonTail.energyGain = 9
dragonTail.save()

ironTail = FastMove(name='Iron Tail', pwr=15, moveType="Steel")
ironTail.energyGain = 7
ironTail.save()

rockThrow = FastMove(name='Rock Throw', pwr=12, moveType="Rock")
rockThrow.energyGain = 7
rockThrow.save()

counter = FastMove(name='Counter', pwr=12, moveType="Fighting")
counter.energyGain = 8
counter.save()

waterfall = FastMove(name='Waterfall', pwr=16, moveType="Water")
waterfall.energyGain = 8
waterfall.save()

razorLeaf = FastMove(name='Razor Leaf', pwr=16, moveType="Grass")
razorLeaf.energyGain = 7
razorLeaf.save()

shadowClaw = FastMove(name='Shadow Claw', pwr=13, moveType="Ghost")
shadowClaw.energyGain = 6
shadowClaw.save()

fireSpin = FastMove(name='Fire Spin', pwr=14, moveType="Fire")
fireSpin.energyGain = 10
fireSpin.save()

confusion = FastMove(name='Confusion', pwr=14, moveType="Psychic")
confusion.energyGain = 15
confusion.save()

poisonJab = FastMove(name='Poison Jab', pwr=10, moveType="Poison")
poisonJab.energyGain = 7
poisonJab.save()

fireFang = FastMove(name='Fire Fang', pwr=11, moveType="Fire")
fireFang.energyGain = 8
fireFang.save()

dragonBreath = FastMove(name='Dragon Breath', pwr=6, moveType="Dragon")
dragonBreath.energyGain = 4
dragonBreath.save()

bite = FastMove(name='Bite', pwr=6, moveType="Dark")
bite.energyGain = 4
bite.save()

scratch = FastMove(name='Scratch', pwr=6, moveType="Normal")
scratch.energyGain = 4
scratch.save()

airSlash = FastMove(name='Air Slash', pwr=14, moveType="Flying")
airSlash.energyGain = 10
airSlash.save()

vineWhip = FastMove(name='Vine Whip', pwr=7, moveType="Grass")
vineWhip.energyGain = 6
vineWhip.save()

pound = FastMove(name='Pound', pwr=7, moveType="Normal")
pound.energyGain = 6
pound.save()

rockSmash = FastMove(name='Rock Smash', pwr=15, moveType="Fighting")
rockSmash.energyGain = 10
rockSmash.save()

metalClaw = FastMove(name='Metal Claw', pwr=8, moveType="Steel")
metalClaw.energyGain = 7
metalClaw.save()

acid = FastMove(name='Acid', pwr=9, moveType="Poison")
acid.energyGain = 8
acid.save()

frostBeath = FastMove(name='Frost Beath', pwr=10, moveType="Ice")
frostBeath.energyGain = 8
frostBeath.save()

feintAttack = FastMove(name='Feint Attack', pwr=10, moveType="Dark")
feintAttack.energyGain = 9
feintAttack.save()

extrasensory = FastMove(name='Extrasensory', pwr=12, moveType="Psychic")
extrasensory.energyGain = 12
extrasensory.save()

zenHeadbutt = FastMove(name='Zen Headbutt', pwr=12, moveType="Psychic")
zenHeadbutt.energyGain = 12
zenHeadbutt.save()

# -------------------------------- ChargedMove ------------------------------- #

precipiceBlades = ChargedMove(
    name='Precipice Blades', pwr=130, moveType="Ground")
precipiceBlades.energyCost = 100
precipiceBlades.save()

originPulse = ChargedMove(name='Origin Pulse', pwr=130, moveType="Water")
originPulse.energyCost = 100
originPulse.save()

doomDespire = ChargedMove(name='Doom Despire', pwr=80, moveType="Steel")
doomDespire.energyCost = 50
doomDespire.save()

braveBird = ChargedMove(name='Brave Bird', pwr=90, moveType="Flying")
braveBird.energyCost = 100
braveBird.save()

futureSight = ChargedMove(name='FutureSight', pwr=120, moveType="Psychic")
futureSight.energyCost = 100
futureSight.save()

closeCombat = ChargedMove(name='Close Combat', pwr=100, moveType="Fighting")
closeCombat.energyCost = 100
closeCombat.save()

stoneEdge = ChargedMove(name='Stone Edge', pwr=100, moveType="Rock")
stoneEdge.energyCost = 100
stoneEdge.save()

petalBlizzard = ChargedMove(name='Petal Blizzard', pwr=110, moveType="Grass")
petalBlizzard.energyCost = 100
petalBlizzard.save()

blizzard = ChargedMove(name='Blizzard', pwr=130, moveType="Ice")
blizzard.energyCost = 100
blizzard.save()

gunkShort = ChargedMove(name='Gunk Short', pwr=130, moveType="Poison")
gunkShort.energyCost = 100
gunkShort.save()

thunder = ChargedMove(name='Thunder', pwr=100, moveType="Electric")
thunder.energyCost = 100
thunder.save()

dracoMeteor = ChargedMove(name='Draco Meteor', pwr=150, moveType="Dragon")
dracoMeteor.energyCost = 100
dracoMeteor.save()

megahorn = ChargedMove(name='Megahorn', pwr=90, moveType="Bug")
megahorn.energyCost = 100
megahorn.save()

huracan = ChargedMove(name='Huracan', pwr=110, moveType="Flying")
huracan.energyCost = 100
huracan.save()

overheat = ChargedMove(name='Overheat', pwr=160, moveType="Fire")
overheat.energyCost = 100
overheat.save()

focusBlast = ChargedMove(name='Focus Blast', pwr=140, moveType="Fighting")
focusBlast.energyCost = 100
focusBlast.save()

hyperBeam = ChargedMove(name='Hyper Beam', pwr=150, moveType="Normal")
hyperBeam.energyCost = 100
hyperBeam.save()

hydroPump = ChargedMove(name='Hydro Pump', pwr=130, moveType="Water")
hydroPump.energyCost = 100
hydroPump.save()

surf = ChargedMove(name='Surf', pwr=65, moveType="Water")
surf.energyCost = 50
surf.save()

zapCannon = ChargedMove(name='Zap Cannon', pwr=140, moveType="Electric")
zapCannon.energyCost = 100
zapCannon.save()

flashCannon = ChargedMove(name='Flash Cannon', pwr=100, moveType="Steel")
flashCannon.energyCost = 100
flashCannon.save()

solarBeam = ChargedMove(name='Solar Beam', pwr=180, moveType="Grass")
solarBeam.energyCost = 100
solarBeam.save()

psychic = ChargedMove(name='Psychic', pwr=100, moveType="Psychic")
psychic.energyCost = 100
psychic.save()

skyAttack = ChargedMove(name='Sky Attack', pwr=100, moveType="Flying")
skyAttack.energyCost = 50
skyAttack.save()

foulPlay = ChargedMove(name='Foul Play', pwr=70, moveType="Dark")
foulPlay.energyCost = 70
foulPlay.save()

# ---------------------------------- players --------------------------------- #

player1 = Player(name='Al', surname='Owen', alias='al_hound', score='0')
player1.save()
player2 = Player(name='Santi', surname='Lopez', alias='sefyrot', score='0')
player2.save()
