import mongoengine
import random
from classes import Player
from datetime import date
from mongoengine.connection import connect
from mongoengine.fields import DateTimeField
from classes import *
from mongoengine import *
from mongoengine import Document
from pprint import pprint

# ------------------------- Connection mongo engine --------------------------- #

uriEng = "mongodb+srv://root:super3@clusterm6.c7eiz.mongodb.net/project?retryWrites=true&w=majority"
clientEng = connect(host=uriEng)

# ---------------------------------- methods --------------------------------- #

# --------------- agrega un Pokemon con un movimiento aleatorio -------------- #

def addPokemon(playerName, poke, t1):
    listaMovCharged = ChargedMove.objects()
    listaMovFast = FastMove.objects()
    player = Player.objects(alias = playerName).first()
    pokCatcheado = Pokemon.objects(name = poke).first()
    if player is None:
        print("El Alias es incorrecto o no existe")
        return
    if pokCatcheado is None:
        print("El pokemon no existe")
        return
    t1.num = pokCatcheado.num
    t1.name = pokCatcheado.name
    t1.ptype = pokCatcheado.ptype
    t1.HPmax = random.uniform(200.0, 1000.0)
    t1.HP = t1.HPmax
    t1.atk = random.randrange(10, 51, 1)
    t1.deff = random.randrange(10, 51, 1)
    t1.CP = (t1.atk+t1.deff+t1.HPmax)
    t1.energy = 0.0
    fastM = listaMovFast[random.randrange(listaMovFast.count())]
    chargedM = listaMovCharged[random.randrange(listaMovCharged.count())]
    movements = move(fast = fastM, charged = chargedM)
    t1.moves = movements
    t1.player = player
    t1.weaknesses = pokCatcheado.weaknesses
    t1.save()
    print("Has añadido a "+pokCatcheado.name)
    
# ---------------------------- dibuja una pokeball --------------------------- #

def pokeball():
    print("────────▄███████████▄────────\n"
          "─────▄███▓▓▓▓▓▓▓▓▓▓▓███▄─────\n"
          "────███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███────\n"
          "───██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██───\n"
          "──██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██──\n"
          "─██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██─\n"
          "██▓▓▓▓▓▓▓▓▓███████▓▓▓▓▓▓▓▓▓██\n"
          "██▓▓▓▓▓▓▓▓██░░░░░██▓▓▓▓▓▓▓▓██\n"
          "██▓▓▓▓▓▓▓██░░███░░██▓▓▓▓▓▓▓██\n"
          "███████████░░███░░███████████\n"
          "██░░░░░░░██░░███░░██░░░░░░░██\n"
          "██░░░░░░░░██░░░░░██░░░░░░░░██\n"
          "██░░░░░░░░░███████░░░░░░░░░██\n"
          "─██░░░░░░░░░░░░░░░░░░░░░░░██─\n"
          "──██░░░░░░░░░░░░░░░░░░░░░██──\n"
          "───██░░░░░░░░░░░░░░░░░░░██───\n"
          "────███░░░░░░░░░░░░░░░███────\n"
          "─────▀███░░░░░░░░░░░███▀─────\n"
          "────────▀███████████▀────────")

def dropteams():
    Team.drop_collection()
    countTeam()

# ------------------- indica la cantidad de pokemon en team ------------------ #

def countTeam():
    numteams = Team.objects(_cls = "Team").count()
    print("Hay", numteams, "pokemons en teams")

# ---------------- indica la cantidad de pokemon en cada team ---------------- #

def countTeams():
    teamA = TeamA.objects().count()
    print("Hay", teamA, "pokemons en team A")
    teamB = TeamB.objects().count()
    print("Hay", teamB, "pokemons en team B")

# ------------------------- coge un pokemon aleatorio ------------------------ #

def randomPokemon():
    pokemons = Pokemon.objects()
    index = random.randint(0, pokemons.count())
    randPokemon = pokemons[index]
    return randPokemon

# ----------------------------- crea los equipos ----------------------------- #

def makeTeams():
    for i in range(6):
        addPokemon("al_hound", randomPokemon().name, TeamA())
    countTeams()
    for i in range(6):
        addPokemon("sefyrot", randomPokemon().name, TeamB())
    countTeams()
    
# -------------------- muestra la informacion del pokemon -------------------- #

def getPokemonInfo(pokemon):
    print(pokemon.name, "HP:", "{:.2f}".format(pokemon.HP), "Energy", "{:.2f}".format(pokemon.energy))

# ---------------------------- funcion del combate --------------------------- #

def combat():
    keepPlaying = True
    teamA = TeamA.objects()
    teamB = TeamB.objects()
    pokemonA = teamA.first()
    pokemonB = teamB.first()
    currentPokemonA = 1
    currentPokemonB = 1
    turn = 1
    while keepPlaying:
        print("\nPokemons Stats:")
        getPokemonInfo(pokemonA)
        getPokemonInfo(pokemonB)
        print("")
        command = input("Attack or Change: ").split()
        if command:
            if command[0].lower() == "attack":
                Attack(pokemonA, pokemonB, turn)
                if pokemonA.HP <= 0:
                    print(pokemonA.name, "has fainted\n")
                    pokemonA.delete()
                    rewardPlayer(pokemonB)
                    if checkTeam(teamA, teamB) == False:
                        pokemonA = teamA.first()
                        print(pokemonA.name, "comes")
                elif pokemonB.HP <= 0:
                    print(pokemonB.name, "has fainted\n")
                    pokemonB.delete()
                    rewardPlayer(pokemonA)
                    if checkTeam(teamA, teamB) == False:
                        pokemonB = teamB.first()
                        print(pokemonB.name, "comes")
                turn = changeTurn(turn)
            elif command[0].lower() == "change":
                if turn == 1:
                    currentPokemonA = changePokemon(teamA, currentPokemonA)
                    pokemonA = teamA[currentPokemonA]
                elif turn == 2:
                    currentPokemonB = changePokemon(teamB, currentPokemonB)
                    pokemonB = teamB[currentPokemonB]
                turn = changeTurn(turn)
            elif command[0].lower() == "run":
                keepPlaying = False
            else:
                print("not a valid command please use 'Attack' or 'Change'")
        else:
            print("not a valid command please use 'Attack' or 'Change'")
        
        if checkTeam(teamA, teamB):
            print("Team A has", teamA.count(), "pokemons left and Team B has", teamB.count(), "pokemons left")
            if teamA.count() > teamB.count():
                print("Team A has won")
            elif teamB.count() > teamA.count():
                print("Team B has won")
            else:
                print("Draw")
            keepPlaying = False

# ------------------ pregunta el tipo de ataque del pokemon ------------------ #

def Attack(pokemonA, pokemonB, turn):
    madeAttack = True
    while madeAttack:
        command = input("Do you want to make a 'Fast' or 'Charged' attack: ").split()
        if command[0].lower() == "fast":
            makeAttack(pokemonA, pokemonB, turn, True)
            madeAttack = False
        elif command[0].lower() == "charged":
            makeAttack(pokemonA, pokemonB, turn, False)
            madeAttack = False
        else:
            print("not a valid command please use 'Fast' or 'Charged'")

# ------------------- hace el ataque basandose en el turno ------------------- #

def makeAttack(pokemonA, pokemonB, turn, fast):
    if turn == 1:
        pokemonB.HP -= attack(pokemonA, pokemonB, fast)
        pokemonB.save()
    elif turn == 2:
        pokemonA.HP -= attack(pokemonB, pokemonA, fast)
        pokemonA.save()
    
# ------------- calcula el daño y lo devuelve para que se aplique ------------ #

def attack(pokemon1, pokemon2, fast):
    energyLeft = 0
    canAttack = True
    damage = 0

    if fast:
        movement = pokemon1.moves.fast.fetch()
        energyLeft = pokemon1.energy + movement.energyGain
        if energyLeft >= 120:
            energyLeft = 120
    else:
        movement = pokemon1.moves.charged.fetch()
        energyLeft = pokemon1.energy - movement.energyCost
        if energyLeft < 0:
            canAttack = False

    print(pokemon1.name, "attacks", pokemon2.name, "with", movement.name)
    if canAttack:
        pokemon1.energy = energyLeft
        pokemon1.save()
        damage = (3 * pokemon1.atk * movement.pwr) / (pokemon2.deff * 2)
        if movement.moveType in pokemon2.weaknesses:
            damage *= 2
        if movement.moveType in pokemon1.ptype:
            damage *= 1.5
        print("the total damage is:", damage, "attack:",pokemon1.atk, "power:", movement.pwr, "def:", pokemon2.deff)
    else:
        print("The attack failed")

    return damage

# ------------------------------ cambia el turno ----------------------------- #

def changeTurn(turn):
    if turn == 1:
        print("\nTeam B's turn:")
        return 2
    elif turn == 2:
        print("\nTeam A's turn:")
        return 1

# ----------------------------- cambia de pokemon ---------------------------- #

def changePokemon(team, current):
    notchanged = True
    poke = 0
    while notchanged:
        index = 0
        print("\nChoose Pokemon")
        for pokemons in team:
            print(index,":", pokemons.name)
            index += 1
        inLine = int(input("write the number: "))
        if inLine < team.count():
            poke = inLine
            notchanged = False
        else:
            print("Not a valid choice")
    return poke

# ---------------- mira si aun quedan pokemons en los equipos ---------------- #

def checkTeam(teamA, teamB):
    if teamA.count() <= 0:
        return True
    elif teamB.count() <= 0:
        return True
    else:
        return False

# ------------------------------- reward player ------------------------------ #

def rewardPlayer(pokemon):
    player = pokemon.player.fetch()
    player.score += 5
    player.save()
    print("The player "+ player.alias+" has been awarded 5 points")