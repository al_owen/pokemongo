from mongoEngMethods import *
from re import split
import pymongo
from pymongo import MongoClient
from pprint import pprint
from pyMongoMethods import *
from mongoEngMethods import *


end = False

while end == False:
    inLine = input("\nWrite a command: ").split()

    # ---------------------------------- verify Input ------------------------------ #
    if inLine:
        if inLine[0].lower() == "end":
            end = True
        elif inLine[0].lower() == "query":
            pokeQuery(inLine)
        elif inLine[0].lower() == "add":
            addPokemon(inLine[1], inLine[2], Team())
            countTeam()
        elif inLine[0].lower() == "detach":
            detach(inLine[1])
        elif inLine[0].lower() == "combat":
            dropteams()
            pokeball()
            makeTeams()
            combat()
        elif inLine[0].lower() == "score":
            addScore(inLine[1])
        else:
            print("not a valid command")
    else:
        print("not a valid command")
